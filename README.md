# Signal隱私與安全強化設定指南（繁體中文版）

## Signal用戶端版本
iOS: Signal v7.48.1

Android: Signal v7.35.0
         Molly v7.33.2-1

## 簡介
Signal是一個被廣泛採納，使用集中式網絡架構的安全即時通訊軟體。它易於使用，且所傳出的所有通訊皆使用了“端到端加密”，並具有接近最小化的元數據，使其成為隱私通訊的優良選擇之一。

該指南描述了如何在盡可能不犧牲可用性的情形下最佳化Signal隱私及安全保護的設定方法。

## 前言-如何進入設定
### iOS:

1.在下方的類別選項（聊天、通話、報導三個類別）中，點選「聊天」。

2.點選左上角自己帳號的頭像。

3.將跳出兩個選項（選擇聊天、設定），選擇「設定」。

### Android:

1.在下方的類別選項（聊天、通話二個類別）中，點選「聊天」。

2.點選左上角自己帳號的頭像。

## Signal通用進階設定

### 1.隱藏電話號碼
1.進入設定後，點選你的個人資料（「帳號」上方）。

2.點選「使用者名稱」。

3.設定自己想要的使用者名稱。

4.返回設定。

5.點選「隱私權」。

6.點選「電話號碼」。

7.將「誰可以看到我的號碼」設定為「iOS:無人、Android:沒有人」。

### 2.Signal PIN
當你註冊Signal時，註冊系統將要求你設定PIN，PIN碼用於解密你儲存在Signal伺服器的帳號，避免你在誤刪或更改設備時損失你的帳號。

另外，這個PIN碼也可充當“註冊鎖”，避免在帳號未登入的情況下被其餘人用相同電話號碼重新註冊而覆蓋原有帳號資料。

#### 啟用「註冊鎖」
##### iOS:
1.進入設定後，點選「帳號」。

2. [X] 開啟「註冊鎖」。

##### Android:

1.進入設定後，點選「帳號」。

2. [X] 開啟「註冊鎖定」。

### 3.隱藏「連結預覽」
#### iOS:

1.進入設定後，點選「聊天」。

2. [ ] 關閉「產生連結預覽」。

#### Android:

1.進入設定後，點選「聊天」。

2. [ ] 關閉「建立連結預覽」。

### 4.關閉「通知顯示聊天內容」

1.進入設定後，點選「通知」。

2.將「顯示」修改為「不顯示名字或內容」。

### 5.開啟「螢幕保護設定」
#### iOS:

1.進入設定後，點選「隱私權」。
 
2. [X] 開啟「在應用程式切換時隱藏螢幕畫面」。
3. [X] 開啟「螢幕鎖定」。

#### Android:

1.進入設定後，點選「隱私權」。

2. [X] 開啟「螢幕鎖定」。
3. [X] 開啟「螢幕安全設定」。

### 6.設定「自動銷毀訊息」

1.進入設定後，點選「隱私權」。

2.點擊「自動銷毀訊息」，選擇自己想要的選項。

### 7.啟用「永遠轉發通話」
1.進入設定後，選擇「隱私權」。

2.點選「進階」。

3. [X] 開啟「永遠轉發通話」。

### 8.密封發件人
「密封發件人」功能使Signal能夠最小化無法加密的元數據，Signal預設對你的所有聯絡人強制啟用此功能，你可依照需要在與你尚未確認為聯絡人的使用者通訊時啟用此功能。

#### 建議

1.進入設定後，選擇「隱私權」。

2.點選「進階」。

3. [X] 開啟「顯示狀態圖示」。

4. [ ] 關閉「允許所有人」。

## iOS版建議
#### 1.關閉「與iOS共享聯絡人」

1.進入設定後，點選「聊天」。

2. [ ] 關閉「與iOS共享聯絡人」。

#### 2.iOS版「隱私權」特有設定

1.進入設定後，點選「隱私權」。

2. [X] 開啟「付款鎖定」。
3. [ ] 關閉「顯示來電於通話紀錄」。

## Android版建議
### 1.Android版「隱私權」特有設定

1.進入設定後，點選「隱私權」。

2. [X] 開啟「隱私鍵盤」。

### 2.Molly

可以考慮使用Molly替代原版Signal客戶端，它擁有原版Signal沒有的安全功能（其中一個強大的功能為「資料庫加密」）。

**警告：使用它意味著你不僅需要信任Signal開發團隊，也需要信任Molly開發團隊；且Molly兩星期才會更新一次（安全性問題會儘快處理），這可能會增加安全風險。**

如果你決定使用Molly，它有以下兩種版本：

1.Molly：如同原版Signal，它並非完全的自由軟體，其中包含有FCM（用於推送通知）、Google maps（用於確認你的位置）這些專有函式庫。

2.Molly-FOSS：大致上如同原版Molly，但它是完全的自由軟體，它使用 Websocket（提示：你可能需要禁用電量最佳化設定才能正常推送通知）和 UnifiedPush 來替代 FCM 推送通知，並使用OpenStreetMap （替代Google maps ）用以確認你的位置。

## Molly特有設定建議

### 1.啟用「資料庫加密」：

1.進入設定後，點選「隱私權」。

2. [X] 開啟「資料庫加密」。

2.輸入你想要設定的密碼（建議：推薦使用Diceware生成一個至少7個單詞的口令短語;或使用密碼生成器生成一個12碼以上的密碼）（不應使用自己發想出的密碼，人的可預測性通常比預想中高）。

## 寫在最後－注意事項

### 避免連結不安全的裝置
雖然Signal具有相當優越的安全性，但作為一個應用程式，它能提供的保護仍有其局限性，因此－你應該只在有足夠強大設備安全性的裝置上使用它，以確保你的通訊足夠安全。

### 安全碼
Signal提供了一個用於驗證你與聯絡人的通訊安全的功能，稱為「安全碼」。你應該與所有聯絡人驗證安全碼。

**（如果安全碼沒有完全吻合，則你不應該繼續使用；或嘗試重新登入並再次進行驗證）。**
#### 驗證方式：

##### iOS

1.在下方的類別（聊天、通話、報導）中，選擇「聊天」。

2.尋找你打算驗證的聯絡人，點入聊天室。

3.進入聊天室後，點選上方聯絡人的頭像。

4.按「檢視安全碼」

5.使用你的備用聊天方式與聯絡人核對安全碼（建議完全核對數字＋掃描QR code）

6.確認完全正確後，按「標記為已驗證」。

##### Android

1.在下方的類別（聊天、通話）中，選擇「聊天」。

2.尋找你打算驗證的聯絡人，點入聊天室。

3.進入聊天室後，點選上方聯絡人的頭像。

4.按「檢視安全碼」。

5.使用你的備用聊天方式與聯絡人核對安全碼（建議完全核對數字＋掃描QR code）

6.確認完全正確後，按「標記為已驗證」。
